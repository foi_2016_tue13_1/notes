# -*- coding: utf-8 -*-
"""
Created on Tue May 17 13:24:05 2016

@author: manb
"""

import pandas as pd

import numpy as np

import os.path
import csv
#just = os.path.join('C:\Documents\notes', 'just dogs_cleaned.csv')

#df=pd.read_csv(just, 'rU', engine='python',header=0,delimiter=",")
#http://pandas.pydata.org/pandas-docs/stable/generated/pandas.read_csv.html#pandas.read_csv


#table = pd.pivot_table(df,values='count',index=['suburb'],columns=['age'],aggfunc=np.sum)
#http://pandas.pydata.org/pandas-docs/stable/generated/pandas.pivot_table.html
#print table

attacks=os.path.join('dogattacks_cleaned.csv')
df2=pd.read_csv(attacks, header=0,delimiter=",")
#df2=pd.read_csv('dog attacks_cleaned.csv', header=0,delimiter=",")
table2= pd.pivot_table(df2, values=['count'], index=['suburb'],columns=['multiple_victims'],aggfunc=np.sum)

table2.columns=table2.columns.droplevel()
table2=table2.fillna(0)
#print table2

with open('pivot dogs.csv', 'rU') as csvfile:
    with open("pivot dogs2.csv", "wb") as file2:
        w=csv.writer(file2)
        reader=csv.reader(csvfile)
        for row in reader:
            if "whippet" in row[1][:15].lower():
                row[1]="Whippet"
            if "wirehaired terrier" in row[1].lower():
                row[1]="Wirehaired Terrier"
            if "wolfhound" in row[1][:15].lower():
                row[1]="Wolfhound"
            if "yorkshire terrier" in row[1][:10].lower():
                row[1]="Yorkshire Terrier"
            
            
            w.writerow(row)
            

"""
for line in f:
    bits = line.split(',')
    # change second column
    bits[1] = '"input"'
    # join it back together and write it out
    fo.write( ','.join(bits) )
    
    
if "death" in row[2]:
                w.writerow([row[0].strip(), row[1].strip(), "death", row[3].strip(), row[4].strip(), row[5].strip(), row[6].strip(), row[7].strip(), row[8].strip(), row[9].strip(), row[10].strip()])
            else:
                w.writerow([row[i].strip() for i in range(len(row))])    
    
    
    """

