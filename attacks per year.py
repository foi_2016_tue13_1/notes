# -*- coding: utf-8 -*-
"""
Created on Sat May 14 15:10:00 2016

@author: TildaThomson

Outputs comma separated lists of the year (as 2 decimals, e.g. "15" for 2015)
and the number of attacks reported in that year.

"""

import csv
from collections import defaultdict

with open('dogattacks_cleaned.csv', 'rU') as csvfile:
    years=defaultdict(int)

    reader=csv.reader(csvfile)
    header=reader.next()
    for row in reader:
        years[row[1][-2:]]+=1
        
    with open("attacks per year.txt", "wb") as file2:
        for key, val in years.items():
            file2.write("['%s', %s]," % (key, val))