# -*- coding: utf-8 -*-
"""
Created on Sat May 14 14:56:34 2016

@author: ben
"""

import csv
from collections import defaultdict

with open('barkingdogs_cleaned.csv', 'rU') as csvfile:
    suburbs=defaultdict(int)

    reader = csv.reader(csvfile)
    header=reader.next()
    for row in reader:
        suburbs[row[0]]=row[5]
    with open("barking dogs per suburb (2015).csv", "wb") as csvfile2:
        w=csv.writer(csvfile2)
        for key, val in suburbs.items():
            w.writerow([key.strip(), val])
