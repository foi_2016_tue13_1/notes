
# -*- coding: utf-8 -*-
"""
Created on Sat May 14 15:10:00 2016

@author: TildaThomson

Outputs a csv file with each row containing the victim name/category, and the
number of attacks on that victim. The output was later 'cleaned' and combined
into fewer categories, resulting in "victims of attacks_aggregated.csv"

"""

import csv
from collections import defaultdict

with open('dogattacks_cleaned.csv', 'rU') as csvfile:
    victims=defaultdict(int)

    reader=csv.reader(csvfile)
    header=reader.next()
    for row in reader:
        victims[row[7]]+=1
        
    with open("victims of attacks.csv", "wb") as csvfile2:
        w=csv.writer(csvfile2)
        for key, val in victims.items():
            w.writerow([key, val])