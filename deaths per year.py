# -*- coding: utf-8 -*-
"""
Created on Sun May 15 14:16:00 2016

@author: TildaThomson

As with bites per year, outputs comma separated lists of the year 
(as two digits) and the number of deaths that occured that year. To be used in 
the column chart that analyses the severity of attacks each year. 

"""

import csv
from collections import defaultdict

with open('dogattacks_cleaned.csv', 'rU') as csvfile:
    years=defaultdict(int)

    reader=csv.reader(csvfile)
    header=reader.next()
    for row in reader:
        if row[4] == 'Y':
            years[row[1][-2:]]+=1
        
    with open("deaths per year.txt", "wb") as file2:
        for key, val in years.items():
            file2.write("['%s', %s]," % (key, val))
    