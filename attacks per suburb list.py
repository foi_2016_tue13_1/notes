# -*- coding: utf-8 -*-
"""
Created on Mon May 09 19:55:15 2016

@author: tilda (adapted from ben)

Tallies the total number of attacks per suburb and outputs as a comma separated 
lists (for easy input into highcharts)
"""

import csv
from collections import defaultdict

with open('dogattacks_cleaned.csv', 'rU') as csvfile:
    suburbs=defaultdict(int)

    reader = csv.reader(csvfile)
    header=reader.next()
    for row in reader:
        suburbs[row[0]]+=1

    with open("dog attacks per suburb_list.text", "w") as file2:
        for key, val in suburbs.items():
            file2.write("['%s', %s]," % (key, val))

    