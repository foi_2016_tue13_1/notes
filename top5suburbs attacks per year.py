# -*- coding: utf-8 -*-
"""
Created on Fri May 20 20:37:04 2016

@author: TildaThomson

Outputs comma separated lists that contain the suburbname and year (combined)
and the number of attacks that occured in that suburb for that year. Uses the
top 5 suburbs identified in the visualisation of attacks per suburb.
"""

import csv
from collections import defaultdict

top5 = ["Corio", "Norlane", "Lara", "Belmont", "Grovedale"]

with open('dogattacks_cleaned.csv', 'rU') as csvfile:
    subs_years=defaultdict(int)

    reader = csv.reader(csvfile)
    header=reader.next()
    for row in reader:
        for suburb in top5:
            if row[0] == suburb:
                #add to dictionary with key of suburb and year (eg "Corio15")
                subs_years[suburb+str(row[1][-2:])]+=1

    with open("top5 attacks per year.text", "w") as file2:
        for key, val in subs_years.items():
            file2.write("['%s', %s],\n" % (key, val))

    