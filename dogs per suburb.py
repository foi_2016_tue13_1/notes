# -*- coding: utf-8 -*-
"""
Created on Sat May 14 13:42:59 2016

@author: TildaThomson

Outputs a csv with each row containing the suburb name and the number of dogs
registered in that suburb. 

"""

import csv
from collections import defaultdict

with open('just dogs_cleaned.csv', 'rU') as csvfile:
    suburbs=defaultdict(int)
    reader=csv.reader(csvfile)
    header=reader.next()
    for row in reader:
        suburbs[row[0]]+=1

    with open("dogs per suburb.csv", "wb") as csvfile2:
        w=csv.writer(csvfile2)
        for key, val in suburbs.items():
            w.writerow([key.strip(), val])