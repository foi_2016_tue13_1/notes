import math
import os
import json

from flask import Flask, request
app = Flask(__name__, static_folder='.', static_url_path='')


@app.route('/ajax-handler', methods=['POST'])
def handler():
  payload = request.data
  
  json_request = json.loads(payload)
  expression = json_request['expression'] # default expression is 1+1 
  # get the data
  via = json_request['format'] # default value is json
  # get the type
  try:
    result = str(eval(expression))
	# deal with the data
  except:
    result = "Failed to evaluate expression"
  # return xml - xml specification
  # define the response 
  if via == "xml":
		body = '<?xml version="1.0" encoding="utf-8"?>\n'
		body += '<response>\n'
		body += '    <expression><![CDATA[%s]]></expression>\n' % (expression)
		body += '    <result>%s</result>\n' % (result)
		body += '</response>\n'
		return body, 200, {'Content-Type': 'text/xml'}
  else:
		body = '{"expression": "%s",' % (expression)
		body += ' "result": "%s"' % (result)
		body += '}' 
		data=json.loads(body)
		return json.dumps(data, separators=(',', ':')), 200, {'Content-Type': 'text/json; charset=utf-8'}

if __name__ == "__main__":
    app.run(debug=True, host='127.0.0.1', port=8763)