# -*- coding: utf-8 -*-
"""
Created on Sun May 15 13:54:39 2016

@author: TildaThomson

Outputs comma separated lists containing the suburb name and the total number
of barking dogs (2011-2015) for that suburb
"""

import csv
from collections import defaultdict

with open('barkingdogs_cleaned.csv', 'rU') as csvfile:
    suburbs=defaultdict(int)

    reader = csv.reader(csvfile)
    header=reader.next()
    for row in reader:
        suburbs[row[0]]+=(int(row[1])+int(row[2])+int(row[3])
        +int(row[4])+int(row[5]))

    with open("barkingdogs per suburb_list.text", "w") as file2:
        for key, val in suburbs.items():
            file2.write("['%s', %s]," % (key, val))
