# -*- coding: utf-8 -*-
"""
Created on Sat May 14 15:32:36 2016

@author: ben

Outputs data in the form that can be placed directly in the highcharts
bubble chart (as keys and values in braces)
x is the total number of dog attacks, y is the total number of barking dogs
and z is the number of dogs registered in that suburb.

"""

import csv
from collections import defaultdict
with open("bubble data.txt", "w") as file:
    with open("dog attacks per suburb.csv", 'rU') as csvfile1:
        suburbs=defaultdict(lambda: [0])
        reader1 = csv.reader(csvfile1)
        header=reader1.next()
        for row in reader1:
            suburbs[row[0]][0]+=1
            suburbs[row[0]].append(row[1])
        with open("barking dogs per suburb (total).csv", 'rU') as csvfile2:
            reader2 = csv.reader(csvfile2)
            header=reader2.next()
            for row in reader2:
                suburbs[row[0]][0]+=1
                suburbs[row[0]].append(row[1])
            with open("dogs per suburb.csv", 'rU') as csvfile3:
                reader3=csv.reader(csvfile3)
                header=reader3.next()
                for row in reader3:
                    suburbs[row[0]][0]+=1
                    suburbs[row[0]].append(row[1])
                for key, val in suburbs.items():
                    if val[0]==3:
                        file.write(
                        "{{x:{0},y:{1},z:{2},suburb:'{3}'}},\n".format(
                            val[1], val[2], val[3], key))