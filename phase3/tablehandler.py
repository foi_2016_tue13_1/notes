import json
import pandas as pd
import numpy as np
from flask import Flask, request
import os.path

portnumber = 8765
pathToPivotDogs = 'pivot dogs2.csv'
pathToPivotAttacks = 'pivot attacks2.csv'


def pivot(x, y, filterval, myfilter, dataset):
    """takes x axis, y axis, value of filter, name of filter and which dataset
    and generates a pivot table in html format"""
    just = os.path.join(pathToPivotDogs)
    attacks = os.path.join(pathToPivotAttacks)
    if dataset == "justdogs":
        df2 = pd.read_csv(just, 'rU', engine='python', header=0, delimiter=",")
        # for API of read_csv, please see background info
    else:
        df2 = pd.read_csv(attacks, header=0, delimiter=",")
    if myfilter == 'year' and dataset != "justdogs":
        df2 = df2[df2.year == filterval]
    elif myfilter == 'bite'and dataset != "justdogs":
        df2 = df2[df2.bite == filterval]
    table2 = pd.pivot_table(df2, values=['count'], index=[str(x)],
                            columns=[str(y)], aggfunc=np.sum)
    # for API of pivot_table, please see background info
    table2.columns = table2.columns.droplevel()  # drop the "count" level
    table2 = table2.fillna(0)
    if y == 'age':
        """ages are numbers and do not sort lexicographically"""
        mylist = []
        for i in table2.columns[:-1]:
            """  "unknown" cannot be cast as an int"""
            mylist.append(int(i))
        mylist.append('Unknown')
        mylist = sorted(mylist)
        mylist = [str(i) for i in mylist]
        table2 = table2.reindex_axis(mylist, axis=1, copy=False)

    return table2.to_html(), 200, {'Content-Type': 'text/html'}

app = Flask(__name__, static_folder='.', static_url_path='')


@app.route("/tablehandler", methods=['POST'])
def table_handler():
    json_request = json.loads(request.data)
    x = json_request['x']
    y = json_request['y']
    filterval = json_request['filterval']
    myfilter = json_request['myfilter']
    dataset = json_request['dataset']
    body = json.dumps(pivot(x, y, filterval, myfilter, dataset))

    return body, 200, {'Content-Type': 'application/json'}

if __name__ == "__main__":
    app.run(debug=True, host='127.0.0.1', port=portnumber)
