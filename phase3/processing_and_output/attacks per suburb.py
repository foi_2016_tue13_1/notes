# -*- coding: utf-8 -*-
"""
Created on Mon May 09 19:55:15 2016

@author: ben
"""

import csv
from collections import defaultdict

with open('dogattacks_cleaned.csv', 'rU') as csvfile:
    suburbs=defaultdict(int)
    reader = csv.reader(csvfile, )
    header=reader.next()
    for row in reader:
        suburbs[row[0]]+=1
    with open("dog attacks per suburb.csv", "wb") as csvfile2:
        w=csv.writer(csvfile2)
        for key, val in suburbs.items():
            w.writerow([key, val])
    