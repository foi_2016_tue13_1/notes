# -*- coding: utf-8 -*-
"""
Created on Thu May 26 10:18:31 2016

@author: TildaThomson

ALTERATION OF bubble data.py (by Ben)
Uses the framework from bubble data.py but instead outputs a list of lists
containing the suburb name and the total dog attacks divided by the total 
number of dogs for that suburb
"""

import csv
from collections import defaultdict

with open("attacks per dog.txt", "w") as file:
    with open("dog attacks per suburb.csv", 'rU') as csvfile1:
        suburbs=defaultdict(lambda: [0])
        reader1 = csv.reader(csvfile1)
        header=reader1.next()
        for row in reader1:
            suburbs[row[0]][0]+=1
            suburbs[row[0]].append(row[1])
        with open("barking dogs per suburb (total).csv", 'rU') as csvfile2:
            reader2 = csv.reader(csvfile2)
            header=reader2.next()
            for row in reader2:
                suburbs[row[0]][0]+=1
                suburbs[row[0]].append(row[1])
            with open("dogs per suburb.csv", 'rU') as csvfile3:
                reader3=csv.reader(csvfile3)
                header=reader3.next()
                for row in reader3:
                    suburbs[row[0]][0]+=1
                    suburbs[row[0]].append(row[1])
                for key, val in suburbs.items():
                    if val[0]==3:
                        file.write("['%s', %f], " % (key, 
                                   (float(val[1])/float(val[3]))))